﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijouBijou.DataAccess;
using BijouBijou.BusinessEntities;
using System.Data;
namespace BijouBijou
{
    public partial class Producto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!Page.IsPostBack ){

                var strProductId = Request.QueryString["ProductId"];
                if (strProductId == "") return;

                int ProductId;
                if (int.TryParse(strProductId, out ProductId))
                {
                    ProductId = int.Parse(strProductId);
                }

                var _productDA = new ProductDA();
                var dsProduct = _productDA.GetProductById(ProductId);
                if (dsProduct.Tables[0].Rows.Count <= 0) return;

                var product = dsProduct.Tables[0].Rows[0];
                
                var productCode = product["ProductCode"].ToString();
                var productDesc = product["ProductDesc"].ToString();
                var productDetail = product["ProductDetail"].ToString();
                var productColor = product["ColorDesc"].ToString();
                var productPrice = product["Price"].ToString();
                var productStock = product["Stock"].ToString();
                var imageCount = int.Parse( product["ImageCount"].ToString());
                var productCategory = product["CategoryDesc"].ToString();

                var infoBuilder = "";
                infoBuilder+="<ul class='list-unstyled'>";

                var details = productDetail.Split('/');
                foreach (var detail in details) {
                    infoBuilder += "<li>- "+detail+"</li>";
                }
                infoBuilder += "<li>- COLOR: " + productColor + "</li>";
                infoBuilder += "<li>- STOCK: " + productStock + "</li>";
                infoBuilder += "</ul>";

                litInfo.Text = infoBuilder;


                litPrecio.Text = "<span class='price'>s/." + productPrice + "</span>";
                litProductDesc.Text = "<h2>" + productDesc + "</h2>";
                
                var imageBuilder = "";
                imageBuilder += "<a href='library/images/" + productCategory + "/600x683/"+productCode+".1.jpg' title='"+productDesc+"' class='cloud-zoom' id='zoom1' rel=''>";
                imageBuilder += "<img src='library/images/" + productCategory + "/550x626/" + productCode + ".1.jpg' title='" + productDesc + "' alt='" + productDesc + "' id='image' />";
                imageBuilder +="</a>";
                litProductImage.Text = imageBuilder;


                var imageAltBuilder = "";
                for(int i = 1; i<=imageCount; i++){
                    imageAltBuilder += "<li class='image-additional'>";
                    imageAltBuilder += "<a href='library/images/" + productCategory + "/600x683/" + productCode + "." + i + ".jpg' title='" + productDesc + "' class='cloud-zoom-gallery'";
                    imageAltBuilder += "rel=\"useZoom:'zoom1', smallImage:'library/images/" + productCategory + "/550x626/" + productCode + "." + i + ".jpg'\">";
                    imageAltBuilder += "<img src='library/images/" + productCategory + "/101x115/" + productCode + "." + i + ".jpg' title='" + productDesc + "' alt='" + productDesc + "' />";
                    imageAltBuilder += "</a>";
                    imageAltBuilder += "</li>";
                }

                litProductAlt.Text = imageAltBuilder;
                GetGrids();      
            }
             
        }

        private void GetGrids()
        {
            var _costoEnvio = new List<CostoEnvio>();
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });

            grdCostoEnvio.DataSource = _costoEnvio;
            grdCostoEnvio.DataBind();


            var _tiempoEnvio = new List<TiempoEnvio>();
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });

            grdTiempoEnvio.DataSource = _tiempoEnvio;
            grdTiempoEnvio.DataBind();
        }
    }
}