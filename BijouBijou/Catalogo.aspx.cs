﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijouBijou.BusinessEntities;
using BijouBijou.DataAccess;
using System.Data;
namespace BijouBijou
{
    public partial class Catalogo : System.Web.UI.Page
    {
         public static List<Product> _listProduct;
         public static string _CategoryName;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {

                GetGrids();
                var strCategoryId = Request.QueryString["CategoryId"];
                if (strCategoryId == "")return;
                
                int CategoryId;
                if (int.TryParse(strCategoryId, out CategoryId)) {
                    CategoryId = int.Parse(strCategoryId);
                }

                
                var _categoryDa = new CategoryDA();
                var dsCategory = _categoryDa.GetCategoryById(CategoryId);

                if (dsCategory.Tables[0].Rows.Count <= 0) return;

                var drCategory = dsCategory.Tables[0].Rows[0];
                _CategoryName = drCategory["CategoryDesc"].ToString();

                var _productDA = new ProductDA();

                _listProduct = _productDA.GetProductsByCategory(CategoryId);

                ProductListConstructor(_listProduct);

            }
        }

        private void GetGrids()
        {
            var _costoEnvio = new List<CostoEnvio>();
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });

            grdCostoEnvio.DataSource = _costoEnvio;
            grdCostoEnvio.DataBind();


            var _tiempoEnvio = new List<TiempoEnvio>();
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });

            grdTiempoEnvio.DataSource = _tiempoEnvio;
            grdTiempoEnvio.DataBind();
        }

        public void inputSort_SelectedIndexChanged(Object sender, EventArgs e)
        {

            if (inputSort.SelectedValue == "1")
                ProductListConstructor(_listProduct);
            else if (inputSort.SelectedValue == "2") {
                var lista = _listProduct.OrderBy(p => p.Price);
                ProductListConstructor(lista.ToList());
            }
            else if (inputSort.SelectedValue == "3")
            {
                var lista = _listProduct.OrderByDescending(p => p.Price);
                ProductListConstructor(lista.ToList());
            }
            
                
        }

        private void  ProductListConstructor(List<Product> productList) {

            if (productList.Count <= 0) litProductList.Text =  "";

            var productListBuilder = "";

            foreach (var product in productList)
            {

                productListBuilder += "<div class='product-layout product-list col-xs-12'>";
                productListBuilder += "<div class='product-thumb'>";
                productListBuilder += "<div class='image'>";
                productListBuilder += "<a href='producto.aspx?ProductId=" + product.ProductId + "'><img src='Library/images/" + _CategoryName + "/270x307/" + product.ProductCode + ".1.jpg' alt='" + product.ProductDesc + "' title='" + product.ProductDesc + "' class='img-responsive' /></a>";
                productListBuilder += "<div class='button-group button-grid'>";
                productListBuilder += "<button class='btn-cart' type='button'><i class='fa fa-shopping-cart'></i> Add to Cart</button>";
                productListBuilder += "</div>";
                productListBuilder += "</div>";
                productListBuilder += "<div class='caption'>";
                productListBuilder += "<div class='name'><a href='producto.html'>" + product.ProductDesc + "</a>";
                productListBuilder += "</div>";
                productListBuilder += "<div class='price'> s/." + product.Price + "</div>";
                productListBuilder += "</div>";
                productListBuilder += "<div class='button-group button-list'>";
                productListBuilder += "<button class='btn-cart' type='button'><i class='fa fa-shopping-cart'></i> Add to Cart</button>";
                productListBuilder += "</div>";
                productListBuilder += "</div>";
                productListBuilder += "</div>";

            }
            litProductList.Text =  productListBuilder;
        }

    }
}