﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijouBijou.BusinessEntities;
using BijouBijou.DataAccess;
using System.Net.Mail;
using System.Configuration;
namespace BijouBijou
{
    public partial class Contactanos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
                CleanFields();
                GetGrids();
            }
        }

        private void GetGrids()
        {
            var _costoEnvio = new List<CostoEnvio>();
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });

            grdCostoEnvio.DataSource = _costoEnvio;
            grdCostoEnvio.DataBind();


            var _tiempoEnvio = new List<TiempoEnvio>();
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });

            grdTiempoEnvio.DataSource = _tiempoEnvio;
            grdTiempoEnvio.DataBind();
        }
        
        private void CleanFields() {
            txtName.Value = "";
            txtEmail.Value = "";
            txtMessage.Value = "";
        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            var contactInfo = new ContactInfo
            {
                Name = txtName.Value,
                LastName ="",
                Email = txtEmail.Value,
                Message = txtMessage.Value
            };
            SendEmail(contactInfo);
            var contactInfoDa = new ContactInfoDA();
            contactInfoDa.SaveContactInfo(contactInfo);
            CleanFields();
            lblSaveMessage.Text = "Su mensaje ha sido enviado.";
        }

        private void SendEmail(ContactInfo contactInfo) {
            var PostEmail = ConfigurationManager.AppSettings["PostEmail"].ToString();
            var PostEmailPassword = ConfigurationManager.AppSettings["PostEmailPassword"].ToString();
            var PostSMTP = ConfigurationManager.AppSettings["PostSMTP"].ToString();
            var PostPort = ConfigurationManager.AppSettings["PostPort"].ToString();

            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(PostEmail, "Bijou Bijou 4u");
            mail.To.Add(PostEmail);
            mail.IsBodyHtml = true;
            mail.Subject = string.Format("Contact Message From: {0} {1}", contactInfo.Name,contactInfo.LastName);
            mail.Body = GetBody(contactInfo);
            mail.Priority = MailPriority.High;

            SmtpClient smtp = new SmtpClient(PostSMTP, int.Parse(PostPort));
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new System.Net.NetworkCredential(PostEmail, PostEmailPassword);
            
            //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

            smtp.Send(mail);
        }

        private string GetBody(ContactInfo contactInfo) {
            var strBody = "";
            strBody += string.Format("Contact: {0} {1}", contactInfo.Name, contactInfo.LastName);
            strBody += "<br/>";
            strBody += string.Format("Email: {0}", contactInfo.Email);
            strBody += "<br/>";
            strBody += "<br/>";
            strBody += "Message:";
            strBody += "<br/>";
            strBody += "<p style='text-align:justify'>"+contactInfo.Message +"</p>";
            return strBody;
        }

            
    }
}