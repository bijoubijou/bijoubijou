﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Producto.aspx.cs" Inherits="BijouBijou.Producto" %>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Bijou Bijou</title>
    <!--<meta name="description" content="Lorem ipsum dolor sit amet, consectetur adipiscing elit.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="HTMLCooker">-->

    <!-- ================= Favicon ================== -->
    <!-- Standard -->
    <link rel="shortcut icon" href="http://placehold.it/64.png/000/fff">
    <!-- Retina iPad Touch Icon-->
    <link rel="apple-touch-icon" sizes="144x144" href="http://placehold.it/144.png/000/fff">
    <!-- Retina iPhone Touch Icon-->
    <link rel="apple-touch-icon" sizes="114x114" href="http://placehold.it/114.png/000/fff">
    <!-- Standard iPad Touch Icon--> 
    <link rel="apple-touch-icon" sizes="72x72" href="http://placehold.it/72.png/000/fff">
    <!-- Standard iPhone Touch Icon--> 
    <link rel="apple-touch-icon" sizes="57x57" href="http://placehold.it/57.png/000/fff">

	<!-- ================= Google Fonts ================== -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400i,300,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,900" rel="stylesheet" type="text/css" />
    <link href='https://fonts.googleapis.com/css?family=Courgette' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,700" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="css/menu_default.css" />
    <link rel="stylesheet" type="text/css" href="css/boss_megamenu.css" />
    <link rel="stylesheet" type="text/css" href="css/jquery.jgrowl.css" />
    <link rel="stylesheet" type="text/css" href="css/boss_alphabet.css" />
    <link rel="stylesheet" type="text/css" href="css/responsive.css" />
    <link rel="stylesheet" type="text/css" href="css/boss_facecomments.css" />
    <link rel="stylesheet" type="text/css" href="css/loading.css" />
    <link rel="stylesheet" type="text/css" href="css/cs.animate.css" />
    <link rel="stylesheet" type="text/css" href="css/boss_special.css" />
    <link rel="stylesheet" type="text/css" href="css/boss_filterproduct.css" />
    <link rel="stylesheet" type="text/css" href="css/isotope.css" />
    <link rel="stylesheet" type="text/css" href="css/bossblog.css" media="screen" />
    <link rel="stylesheet" type="text/css" href="css/revolutionslider_settings.css" media="screen" />

	<!--[if lt IE 9]>
	  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	  <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <link href="css/StyleModal.css" rel="stylesheet" />
</head>

<body class="bt-home-page" style="background-color:#d9d9d9">
   
     <!--<div id="bt_loading">
        <div class="bt-loading">
            <div id="circularG">
                <div id="circularG_1" class="circularG"> </div>
                <div id="circularG_2" class="circularG"> </div>
                <div id="circularG_3" class="circularG"> </div>
                <div id="circularG_4" class="circularG"> </div>
                <div id="circularG_5" class="circularG"> </div>
                <div id="circularG_6" class="circularG"> </div>
                <div id="circularG_7" class="circularG"> </div>
                <div id="circularG_8" class="circularG"> </div>
            </div>
        </div>
    </div>--><!-- /#bt_loading -->
    <div id="bt_container" class="bt-wide">
        <div id="bt_header">
            <nav id="top">
                <div class="container">
                    <div class="row">
                        <div id="right_top_links" class="nav pull-right">
                            <!--<div class="bt-language">
                                <form method="post" enctype="multipart/form-data" class="language">
                                    <div class="btn-group">
                                        <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"> <span><img src="images/flags/gb.png" alt="English" title="English"></span> <span>en</span><i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li><a href="en"><span class="text-left">English</span><span class="text-right"><img src="images/flags/gb.png" alt="English" title="English" /></span></a>
                                            </li>
                                            <li><a href="de"><span class="text-left">Deutsch</span><span class="text-right"><img src="images/flags/de.png" alt="Deutsch" title="Deutsch" /></span></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="code" value="" />
                                    <input type="hidden" name="redirect" value="" /> </form>
                            </div>--><!-- /.bt-language -->
                            <!--<div class="bt-currency">
                                <form method="post" enctype="multipart/form-data" class="currency">
                                    <div class="btn-group">
                                        <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"> <strong>$</strong> <span>USD</span> <i class="fa fa-angle-down"></i>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <button class="currency-select btn btn-link" type="button" name="EUR"><span>Euro</span> <span>€</span>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="currency-select btn btn-link" type="button" name="GBP"><span>Pound Sterling</span> <span>£</span>
                                                </button>
                                            </li>
                                            <li>
                                                <button class="currency-select btn btn-link" type="button" name="USD"><span>US Dollar</span> <span>$</span>
                                                </button>
                                            </li>
                                        </ul>
                                    </div>
                                    <input type="hidden" name="code" value="" />
                                    <input type="hidden" name="redirect" value="" /> </form>
                            </div>--><!-- /.bt-currency -->
                            <ul class="list-inline">
                                <li><a href="wishlist.html" id="wishlist-total" title="Wish List (0)"><span><i class="fa fa-heart"></i> Wish List (0)</span></a>
                                </li>
                                <li class="dropdown"><a href="#" title="My Account" class="dropdown-toggle" data-toggle="dropdown"><span><i class="fa fa-user"></i>Mi Cuenta</span><i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li><a href="register.html"><span>Registrarse</span><span><i class="fa fa-unlock-alt"></i></span></a>
                                        </li>
                                        <li><a href="login.html"><span>Ingresar</span><span><i class="fa fa-user"></i></span></a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="row">
                    <div class="bt-content-menu" style="float: left; width: 100%; clear: both; height: 1px;"></div>
                </div>
            </div> <a class="open-bt-mobile"><i class="fa fa-bars"></i></a>
            <div class="bt-mobile">
                <div class="menu_mobile"> <a class="close-panel"><i class="fa fa-times-circle"></i></a>
                    <!--<div class="bt-language">
                        <form method="post" enctype="multipart/form-data" class="language">
                            <div class="btn-group">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"> <span><img src="images/flags/gb.png" alt="English" title="English"></span> <span>en</span><i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li><a href="en"><span class="text-left">English</span><span class="text-right"><img src="images/flags/gb.png" alt="English" title="English" /></span></a>
                                    </li>
                                    <li><a href="de"><span class="text-left">Deutsch</span><span class="text-right"><img src="images/flags/de.png" alt="Deutsch" title="Deutsch" /></span></a>
                                    </li>
                                </ul>
                            </div>
                            <input type="hidden" name="code" value="" />
                            <input type="hidden" name="redirect" value="" /> </form>
                    </div>--><!-- /.bt-language -->
                   <!-- <div class="bt-currency">
                        <form method="post" enctype="multipart/form-data" class="currency">
                            <div class="btn-group">
                                <button class="btn btn-link dropdown-toggle" data-toggle="dropdown"> <strong>$</strong> <span>USD</span> <i class="fa fa-angle-down"></i>
                                </button>
                                <ul class="dropdown-menu">
                                    <li>
                                        <button class="currency-select btn btn-link" type="button" name="EUR"><span>Euro</span> <span>€</span>
                                        </button>
                                    </li>
                                    <li>
                                        <button class="currency-select btn btn-link" type="button" name="GBP"><span>Pound Sterling</span> <span>£</span>
                                        </button>
                                    </li>
                                    <li>
                                        <button class="currency-select btn btn-link" type="button" name="USD"><span>US Dollar</span> <span>$</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                            <input type="hidden" name="code" value="" />
                            <input type="hidden" name="redirect" value="" /> </form>
                    </div>--><!-- /.bt-currency -->
                    <div class="logged-link"> <a href="login.html"><i class="fa fa-sign-in"></i><span>Sign In</span></a> <a href="register.html"><i class="fa fa-hand-o-up"></i><span>Join for Free</span></a> </div>
                </div>
            </div><!-- /.bt-mobile -->
            <header>
                <div class="container">
                    <div class="row">
                        <!--<div class="col-sm-3">-->
                        <div id="logo" style="width:280px">
                            <div style="float:left;width:30%">&nbsp;</div>
                            <div style="float:left;width:50%">
                                <a href="index.aspx"><img src="images/logo.png" title="Bijou Bijou" alt="Bijou Bijou" class="img-responsive" />
                                    </a>
                            </div>
                            <div style="clear:both"></div>
                        </div>
                        <!-- </div>-->
                        <div id="cart" class="btn-group btn-block">
                            <button type="button" data-toggle="dropdown" data-loading-text="Loading..." class="btn btn-inverse btn-block btn-lg dropdown-toggle"><i class="fa fa-shopping-cart"></i> <span id="cart-total">2 item(s) - $316.00</span>
                                    </button>
                                    <ul class="dropdown-menu pull-right">
                                        <li>
                                            <table class="table table-striped">
                                                <tr>
                                                    <td class="text-left image">
                                                        <div class="image">
                                                            <a href="producto.html"><img src="images/product/p21-115x131.jpg" alt="Cashmere cuff vintage Levi maxi" title="Cashmere cuff vintage Levi maxi" class="img-thumbnail" />
                                                            </a>
                                                            <div class="remove">
                                                                <button type="button" title="Remove" class="btn-danger"><i class="fa fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="text-left name">
                                                        <a href="producto.html">Cashmere cuff vintage Levi maxi</a>
                                                        <br /> - <small>Color Pink</small>
                                                        <div>2 x <span class="price">$316.00</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </li>
                                        <li>
                                            <div class="cart_bottom">
                                                <table class="minicart_total">
                                                    <tr>
                                                        <td class="text-left"><span>Sub-Total</span>
                                                        </td>
                                                        <td class="text-right">$260.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left"><span>Eco Tax (-2.00)</span>
                                                        </td>
                                                        <td class="text-right">$4.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left"><span>VAT (20%)</span>
                                                        </td>
                                                        <td class="text-right">$52.00</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="text-left"><span>Total</span>
                                                        </td>
                                                        <td class="text-right">$316.00</td>
                                                    </tr>
                                                </table>
                                                <div class="buttons"> 
                                                    <span class="cart_bt"><a href="cart.html" class="btn">Ver Carrito</a></span> 
                                                    <span class="checkout_bt"><a class="btn btn-shopping" href="checkout.html">Checkout</a></span> 
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                        </div><!-- /#cart -->
                        <div class="bt-block-call-us">
                            <a href="#"><img src="images/support.jpg" alt="call us">
                            </a>
                            <p>Llámenos ahora</p> <span>(51) 1 205 0000</span> 
                        </div><!-- /.bt-block-call-us -->
                        <div id="boss-search" class="fourteen columns omega">
                            <div class="choose-select">
                                <div class="input_cat">
                                    <select name="filter_category_id" id="boss_filter_search">
                                        <option value="0" selected="selected">Todas las Categorías</option>
                                        <option value="">Collares</option>
                                        <option value="">Pulseras</option>
                                        <option value="">Aretes</option>
                                        <option value="">Tobilleras</option>
                                    </select>
                                </div>
                                <div class="search-form">
                                    <div id="search" class="input-group">
                                        <input type="text" name="search" value="" placeholder="Buscar aquí..." class="form-control input-lg" /> 
                                        <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button>
                                      </span> </div>
                                </div>
                            </div>
                        </div><!-- /#boss-search -->
                    </div>
                </div>
            </header>
            <div class="boss-new-position">
                <div class="boss_header"></div>
                <div class="container">
                    <div class="menu_custom row">
                        <div class="menu">
                            <!-- Load menu -->
                            <div class="menubar">
                                <div class="container">
                                    <div class="bt_mainmenu">
                                        
                                    </div>
                                </div>
                            </div><!-- /.menubar -->
                        </div><!-- /.menu -->
                        <div class="header_category">
                            <div id="boss-menu-category" class="box">
                                <div class="boss_heading">
                                    <div class="box-heading"><span>Categorías</span><span></span>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <ul class="box-category boss-menu-cate">
                                        <li>
                                            <div class="nav_title"> <img alt="Collares" src="images/icon/icono_collar.png" /> <a class="title" href="Catalogo.aspx?CategoryId=1">Collares</a> </div>
                                            <div class="nav_submenu" >
                                                <div class="nav_submenu_inner" style="width:200px;">
                                                    <div class="nav_sub_submenu">
                                                        <ul>
                                                            <li class="nav_cat_parent" style="width:166.66666666667px;">
                                                                <div class="sub_cat_child">
                                                                    <ul>
                                                                        <li><a href="#">Collares Cortos</a>
                                                                        </li>
                                                                        <li><a href="#">Collares Largos</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="nav_title"> <img alt="Pulseras y Brazaletes" src="images/icon/icono_pulsera.png" /> <a class="title" href="Catalogo.aspx?CategoryId=2">Pulseras y Brazaletes</a> </div>
                                            <div class="nav_submenu" >
                                                <div class="nav_submenu_inner" style="width:200px;">
                                                    <div class="nav_sub_submenu">
                                                        <ul>
                                                            <li class="nav_cat_parent" style="width:166.66666666667px;">
                                                                <div class="sub_cat_child">
                                                                    <ul>
                                                                        <li><a href="#">Pulseras</a>
                                                                        </li>
                                                                        <li><a href="#">Brazaletes</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                       <%--<li>
                                            <div class="nav_title"> <img alt="Aretes" src="images/icon/icono_aretes.png" /> <a class="title" href="Catalogo.aspx?CategoryId=3">Aretes</a> </div>
                                        </li>--%>
                                       <li>
                                            <div class="nav_title"> <img alt="Tobilleras" src="images/icon/icono_tob.png" /> <a class="title" href="Catalogo.aspx?CategoryId=4">Tobilleras</a> </div>
                                        </li>
                                    </ul>
                                </div><!-- /.box-content -->
                            </div>
                        </div><!-- /.header_category -->
                        <div class="header_category menucategory-fixed">
                            <div id="boss-menu-category-fixed" class="box">
                                <div class="boss_heading">
                                    <div class="box-heading"><span>Categorías</span><span></span>
                                    </div>
                                </div>
                                <div class="box-content">
                                    <ul class="box-category boss-menu-cate">
                                        <li>
                                            <div class="nav_title"> <img alt="Collares" src="images/icon/icono_collar.png" /> <a class="title" href="Catalogo.aspx?CategoryId=1">Collares</a> </div>
                                            <div class="nav_submenu" >
                                                <div class="nav_submenu_inner" style="width:200px;">
                                                    <div class="nav_sub_submenu">
                                                        <ul>
                                                            <li class="nav_cat_parent" style="width:166.66666666667px;">
                                                                <div class="sub_cat_child">
                                                                    <ul>
                                                                        <li><a href="#">Collares Cortos</a>
                                                                        </li>
                                                                        <li><a href="#">Collares Largos</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="nav_title"> <img alt="Pulseras y Brazaletes" src="images/icon/icono_pulsera.png" /> <a class="title" href="Catalogo.aspx?CategoryId=2">Pulseras y Brazaletes</a> </div>
                                            <div class="nav_submenu" >
                                                <div class="nav_submenu_inner" style="width:200px;">
                                                    <div class="nav_sub_submenu">
                                                        <ul>
                                                            <li class="nav_cat_parent" style="width:166.66666666667px;">
                                                                <div class="sub_cat_child">
                                                                    <ul>
                                                                        <li><a href="#">Pulseras</a>
                                                                        </li>
                                                                        <li><a href="#">Brazaletes</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                       <%--<li>
                                            <div class="nav_title"> <img alt="Aretes" src="images/icon/icono_aretes.png" /> <a class="title" href="Catalogo.aspx?CategoryId=3">Aretes</a> </div>
                                        </li>--%>
                                       <li>
                                            <div class="nav_title"> <img alt="Tobilleras" src="images/icon/icono_tob.png" /> <a class="title" href="Catalogo.aspx?CategoryId=4">Tobilleras</a> </div>
                                        </li>
                                    </ul>
                                </div><!-- /.box-content -->
                            </div>
                        </div><!-- /.header_category -->
                        <div class="bt-staticblock-freeshipping">
                            &nbsp;
                        </div><!-- /.bt-staticblock-freeshipping -->
                        
                    </div><!-- /.menu_custom -->
                </div>
            </div><!-- /.boss-new-position -->
        </div><!-- /#bt_header -->
        
        <div class="container">
            <div class="row">
                <column id="column-left" class="col-sm-3 hidden-xs">
                </column><!-- /#column-left -->
                <div id="content" class="col-sm-9">
                    <div class="product-info">
                        <div class="row">
                            <div class="col-sm-6 ">
                                <div class="bt-product-zoom">
                                    <div class="bosszoomtoolbox boss-additional">
                                        <div itemprop="image" class="image">
                                           <asp:Literal runat="server" ID="litProductImage"></asp:Literal>
                                        </div>
                                        <div class="image-additional">
                                            <ul class="thumbnails" id="boss-image-additional">
                                                <asp:Literal runat="server" ID="litProductAlt"></asp:Literal>
                                            </ul> <a id="prev_image_additional" class="prev nav_thumb" href="javascript:void(0)" style="display:block;" title="prev"><i data-original-title="Previous" class="fa fa-angle-left btooltip">&nbsp;</i></a> <a id="next_image_additional" class="next nav_thumb" href="javascript:void(0)" style="display:block;" title="next"><i data-original-title="Next" class="fa fa-angle-right btooltip">&nbsp;</i></a> 
                                        </div><!-- /.image-additional -->
                                    </div><!-- /.bosszoomtoolbox --> 
                                </div><!-- /.bt-product-zoom -->
                            </div>
                            <div class="col-sm-6">
                                <asp:Literal runat="server" ID="litProductDesc"></asp:Literal>
                                <div class="description">
                                    <h3>INFO</h3>
                                    <asp:Literal runat="server" ID="litInfo"></asp:Literal>
                                </div><!-- /.description -->
                                <div class="price_info"> 
                                    <asp:Literal runat="server" ID="litPrecio"></asp:Literal>
                                    <br> 
                                </div><!-- /.price_info -->
                                <div id="product" class="options">
                                    <h3>Available Options</h3>
                                    <div class="cart">
                                        <div class="quantily_info">
                                            <div class="title_text"><span>Ctd.</span>
                                            </div>
                                            <div class="select_number">
                                                <input type="text" name="quantity" value="1" size="2" id="input-quantity" class="form-control" />
                                                <button onclick="changeQty(1); return false;" class="increase"><i class="fa fa-angle-up"></i>
                                                </button>
                                                <button onclick="changeQty(0); return false;" class="decrease"><i class="fa fa-angle-down"></i>
                                                </button>
                                            </div>
                                            <input type="hidden" name="product_id" size="2" value="32" /> </div>
                                        <div class="button-group">
                                            <button type="button" id="button-cart" data-loading-text="Cargando..." class="btn-cart"><i class="fa fa-shopping-cart"></i> Añadir al carrito</button>
                                        </div>
                                    </div>
                                </div><!-- /.options -->
                            </div>
                        </div>
                    </div><!-- /.product-info -->
                </div><!-- /#content -->
                <div id="tags-load"><i class="fa fa-spinner fa-pulse fa-2x"></i>
                </div>
            </div><!-- /.row -->
        </div>
        
            <div class="container">
            <div class="row">
                <div class="bt-staticblock-shipping">
                   <div class="bt-staticblock"> <a href="#" class="bt-shipping-icon"><i class="fa fa-tag"></i></a>
                        <h3> <a href="#">DESCUENTO POR COMPRAS</a></h3>
                        <p>Por compras superiores a los S/. 100.00 recibirás un código para el descuento de S/. 10.00 que podrás utilizar en tu siguiente compra. También puedes ganar descuentos por compras de tus amigos/amigas … Para mayor información, 
                            <a id="opener">haz click aquí!! </a></p>
                    </div>
                   <div class="bt-staticblock"> <a href="#" class="bt-shipping-icon"><i class="fa fa-truck"></i></a>
                        <h3><a href="#">COSTO DE ENVIO</a>   </h3>
                        <p>Por compras mayores a S/. 150.00, el envío es gratuito. Revisa cuáles son las zonas de reparto y el costo, 
                            <a id="openerCostoEnvio">haz click aquí!! </a></p>
                    </div>
                     <div class="bt-staticblock"> <a href="#" class="bt-shipping-icon"><i class="fa fa-paper-plane"></i></a>
                        <h3><a href="#">ENVIO</a> </h3>
                        <p>La entrega de tus productos serán de 2 a 5 días como máximo!!!! Desde la fecha que se realiza el pago. En caso necesites que tu entrega se realice en menor tiempo o un día determinado, 
                            <a id="openerEnvio">haz click aquí!! </a></p>
                    </div>
                    
                </div>
            </div>
        </div><!-- /.container -->
        <!-- MODAL POPUP DESCUENTOS -->
        <form runat="server">
        <div id="dialog" title="Descuentos Por Compras" style="text-align:justify;font-size: 70%;" >
          <p>
            Por compras superiores a los S/. 100.00 recibirás un cupón para el descuento de S/. 10.00 que podrás utilizar en tu siguiente compra.
          </p>
          <p>
            Adicionalmente, por la primera compra de tus amigos/amigas que indiquen que entraron en nuestra web por recomendación tuya, te harás ganador de un cupón de descuento de S/. 10.00.
          </p>
          <p>
            Los cupones no son acumulables, es decir, en tu compra sólo podrás utilizar un cupón, pero si tienes más, se quedarán en tu cuenta !esperándote! para que los puedas usar en tu siguiente compra!!!!! 
          </p>
        </div>
         <!-- MODAL POPUP COSTO POR ENVIOS -->
        <div id="dialogCostoEnvio" title="Costo de Envío" style="text-align:justify;font-size: 70%;">
          <p>
            Por compras mayores a S/.150.00 tendrás el envío gratuito de tu pedido, en cualquiera de nuestras zonas de reparto. Caso contrario, revisa el listado para ver el costo.
          </p>
          <div>
            <asp:GridView runat="server" ID="grdCostoEnvio" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField runat="server" HeaderText="Zona de Reparto" DataField="ZonaReparto">
                    <ItemStyle Width="120px" />
                    </asp:BoundField>
                    <asp:BoundField runat="server" HeaderText="Costo" DataField="Costo">
                        <ItemStyle Width="50px" />
                    </asp:BoundField>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#E2B1B3" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
          </div>  
        </div>
        <!-- MODAL POPUP ENVIO -->
        <div id="dialogEnvio" title="Envío" style="text-align:justify;font-size: 70%;">
          <p>
            El tiempo de entrega regular de tu pedido será de 2 a 5 días como máximo desde que realizaste el pago. Si es necesario que tu pedido llegue en una fecha determinada o en un plazo menor, revisa el cuadro de precios para el delivery especial que está a continuación:
          </p>
            <div>
            <asp:GridView runat="server" ID="grdTiempoEnvio" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField runat="server" HeaderText="Zona de Reparto" DataField="ZonaReparto">
                    <ItemStyle Width="120px" />
                    </asp:BoundField>
                    <asp:BoundField runat="server" HeaderText="Días" DataField="Tiempo">
                        <ItemStyle Width="50px" />
                    </asp:BoundField>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#E2B1B3" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
          </div>  
        </div>
        </form>
        <div id="bt_footer">
            <footer>
                <div class="bt-footer-middle">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="bt-block-footer col-sm-4 col-xs-12">
                                    <div class="col-xs-12">
                                        <a href="index.aspx"  style="padding-left:110px"><img src="images/logo_footer.png" alt="BijouBijou" title="BijouBijou" />
                                        </a>
                                        <p>Bijou Bijou 4u es una marca para las mujeres con estilo a la vanguardia de las tendencias en joyas. Si para la oficina, el tiempo libre, una salida de noche o a la luz del día, ropa formal o más casual, si vestidos o polos, todo tipo de estilo para cualquier ocasión en tus manos en poco tiempo. Ofrecemos delivery o puntos de encuentro para la entrega de tu joya.</p>
                                        <div class="footer-contact">
                                            <ul>
                                                <%--<li><i class="fa fa-map-marker"></i> <span> &nbsp;Av. Arenales 450, Jesús María</span>
                                                </li>--%>
                                                <li><i class="fa fa-phone"></i> <span>(51) 1 205 0000</span>
                                                </li>
                                                <li><i class="fa fa-envelope-o"></i> <span><a href="mailto:consultas@bijoubijou4u.com.pe">consultas@bijoubijou4u.com.pe</a></span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div><!-- /.col-sm-4 -->
                                <div class="col-sm-4 col-xs-12">
                                    <!--Load modules in position footer middle-->
                                    <div class="footer-social col-xs-4">
                                    <h3>Síguenos</h3>
                                    <ul>
                                        <li><a class="facebook" target="_blank" href="https://www.facebook.com/bijoubijou4u/" title="Facebook"><i class="fa fa-facebook"></i></a>
                                        </li>
                                    </ul>
                                </div><!-- /.col-sm-4 -->
                                    <!-- /.box -->
                                </div><!-- /.col-sm-4 -->
                                <div class="col-sm-4 col-xs-12">
                                    <!--Load modules in position footer newsletter-->
                                    <div class="bt-newsletter">
                                        <div class="footer-newsletter">
                                            <div class="title">
                                                <h3>Suscríbete al newsletter</h3>
                                                <p>Ingresa tu correo para enviarte las promociones.</p>
                                            </div>
                                            <div>
                                                <div class="newsletter-content">
                                                    <div id="frm_subscribe">
                                                        <form name="subscribe" id="subscribe">
                                                            <table>
                                                                <tr>
                                                                    <td>
                                                                        <div class="boss-newsletter">
                                                                            <input class="form-control input-new" size="50" type="text" placeholder="Ingrese su correo aquí" name="subscribe_email" id="subscribe_email"> <a class="btn btn-new"><i class="fa fa-paper-plane"></i></a>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr style="display:none;">
                                                                    <td>
                                                                        <input class="form-control input-new" size="50" type="text" value="Ingrese su correo aquí" name="subscribe_name" id="subscribe_name">
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td id="subscribe_result"></td>
                                                                </tr>
                                                            </table>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- /.col-sm-4 -->
                                
                            </div>
                        </div>
                    </div>
                </div><!-- /.bt-footer-middle -->
                <div class="bt-footer-bottom">
                    <div class="container">
                        <div class="row">
                            <div class="link">
                                <ul class="list-unstyled">
                                    <li><a href="contactanos.aspx">Contáctanos</a>
                                    </li>
                                    <li><a href="contactanos.aspx">Preguntas Frecuentes</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="powered-payment">
                                <div class="row">
                                    <div class="powered col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div id="powered">
                                            <!--<p>© 2015 Fashion Store. All rights Reserved.<span> Themes by <a href="http://htmlcooker.com">HTMLCooker</a>.</span>-->
                                            <!--</p>-->
                                        </div>
                                    </div>
                                    <div class="payment col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <ul>
                                            <li>
                                                <a title="Visa" href="#" class="visa"><img alt="Visa" src="images/visa.jpg">
                                                </a>
                                            </li>
                                            <li>
                                                <a title="MasterCard" href="#" class="masterCard"><img alt="MasterCard" src="images/master_card.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Paypal" href="#" class="paypal"><img alt="Paypal" src="images/paypal.jpg">
                                                </a>
                                            </li>
                                            <li>
                                                <a title="Merican" href="#" class="merican"><img alt="Merican" src="images/american.jpg" />
                                                </a>
                                            </li>
                                            <li>
                                                <a title="DHL" href="#" class="dhl"><img alt="DHL" src="images/dhl.jpg">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div><!-- /.powered-payment -->
                        </div>
                    </div>
                </div><!-- /.bt-footer-bottom -->
            </footer>
        </div><!-- /#bt_footer -->
        <div id="back_top" class="back_top" title="Back To Top"><span><i class="fa fa-angle-up"></i></span></div>

    </div>

   
     <script src="js/jquery-2.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/getwidthbrowser.js"></script>
    <script src="js/cs.bossthemes.js"></script>
    <script src="js/jquery.jgrowl.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/jquery.smoothscroll.js"></script>
    <script src="js/carouFredSel-6.2.1.js"></script>
    <script src="js/boss_filterproduct.js"></script>
    <script src="js/isotope.pkgd.js"></script>
    <script src="js/plugins.js"></script>
    <script src="js/jquery.tools.min.js"></script>
    <script src="js/jquery.revolution.min.js"></script>
    <script src="js/jquery.selectbox-0.2.min.js"></script>
    <script src="js/cloud-zoom.1.0.3.js"></script>
    <script src="js/custom.js"></script>
    <script>
        jQuery(document).ready(function () {

            boss_quick_shop();
            dataAnimate();

            /* View Mode */
            // Product List
            $('#list-view').on('click', function () {
                $('#content .product-layout').attr('class', 'product-layout product-list col-xs-12');
                localStorage.setItem('display', 'list');
            });
            // Product Grid
            $('#grid-view').on('click', function () {
                // What a shame bootstrap does not take into account dynamically loaded columns
                cols = $('#column-right, #column-left').length;
                if (cols == 2) {
                    $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-6 col-xs-12');
                } else if (cols == 1) {
                    $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-4 col-xs-12');
                } else {
                    $('#content .product-layout').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-3 col-xs-12');
                }
                localStorage.setItem('display', 'grid');
            });
            if (localStorage.getItem('display') == 'list') {
                $('#list-view').trigger('click');
            } else {
                $('#grid-view').trigger('click');
            }


            /* boss-image-additional slider */
            $('#boss-image-additional').carouFredSel({
                auto: false,
                responsive: true,
                width: '100%',
                prev: '#prev_image_additional',
                next: '#next_image_additional',
                swipe: {
                    onTouch: true
                },
                items: {
                    width: 120,
                    height: 'auto',
                    visible: {
                        min: 1,
                        max: 3
                    }
                },
                scroll: {
                    direction: 'left', //  The direction of the transition.
                    duration: 500, //  The duration of the transition.
                }
            });

        });
        /* Modal Quick Shop */
        $('#myModal').on('shown.bs.modal', function (e) {
            $.fn.CloudZoom.defaults = {
                adjustX: 0,
                adjustY: 0,
                tint: '#FFF',
                tintOpacity: 0.5,
                softFocus: 0,
                lensOpacity: 0.7,
                zoomWidth: '450',
                zoomHeight: '552',
                position: 'inside',
                showTitle: 0,
                titleOpacity: 0.5,
                smoothMove: '3'
            };
            $('.cloud-zoom, .cloud-zoom-gallery').CloudZoom();
        })
        /* Quick Shop */
        function boss_quick_shop() {
            $('.product-thumb').each(function (index, value) {
                var _qsHref = '<button class=\"btn-quickshop\" title =\"Quick Shop\" class=\"sft_quickshop_icon \" data-toggle=\"modal\" data-target=\"#myModal\"><i class="fa fa-eye"></i></button>';
                $('.button-group', this).append(_qsHref);
            });
        }

        function changeQty(increase) {
            var qty = parseInt($('.select_number').find("input").val());
            if (!isNaN(qty)) {
                qty = increase ? qty + 1 : (qty > 1 ? qty - 1 : 1);
                $('.select_number').find("input").val(qty);
            } else {
                $('.select_number').find("input").val(1);
            }
        }
        function goToByScroll(id) {
            $('html,body').animate({ scrollTop: $("#" + id).offset().top }, 'slow');
        }
    </script>

    <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script>
        $(function () {
            $("#dialog").dialog({
                autoOpen: false,
                height: 220,
                width: 480,
                modal: true,
            });

            $("#dialogCostoEnvio").dialog({
                autoOpen: false,
                height: 220,
                width: 480,
                modal: true,
            });

            $("#dialogEnvio").dialog({
                autoOpen: false,
                height: 220,
                width: 480,
                modal: true,
            });


            $("#opener").click(function () {
                $("#dialog").dialog("open");
            });

            $("#openerCostoEnvio").click(function () {
                $("#dialogCostoEnvio").dialog("open");
            });

            $("#openerEnvio").click(function () {
                $("#dialogEnvio").dialog("open");
            });

        });
  </script>
</body>
</html>