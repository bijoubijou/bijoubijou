﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BijouBijou.BusinessEntities;
namespace BijouBijou
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack) {
                GetGrids();
            }
            
        }

        private void GetGrids() {
            var _costoEnvio = new List<CostoEnvio>();
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });
            _costoEnvio.Add(new CostoEnvio { ZonaReparto = "Lima", Costo = "10" });

            grdCostoEnvio.DataSource = _costoEnvio;
            grdCostoEnvio.DataBind();


            var _tiempoEnvio = new List<TiempoEnvio>();
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });
            _tiempoEnvio.Add(new TiempoEnvio { ZonaReparto = "Lima", Tiempo = "2" });

            grdTiempoEnvio.DataSource = _tiempoEnvio;
            grdTiempoEnvio.DataBind();
        }
    }
}