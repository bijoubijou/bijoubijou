﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data.SqlClient;

namespace BijouBijou.DataAccess
{
    public class Conexion
    {
        public SqlConnection conectar() {
            SqlConnection conexion = new SqlConnection();

            ConnectionStringSettings settings;
            settings = System.Configuration.ConfigurationManager.ConnectionStrings["conexion"];
            string connectionString = settings.ConnectionString;
            conexion.ConnectionString = connectionString;

            return conexion;
        }
    }
}
