﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BijouBijou.BusinessEntities;

namespace BijouBijou.DataAccess
{
    public class ContactInfoDA
    {
        Conexion con = new Conexion();

        public void SaveContactInfo(ContactInfo contactInfo)
        {
            SqlConnection conexion = new SqlConnection();
            conexion = con.conectar();

            SqlDataAdapter daCategory = new SqlDataAdapter("uspSaveContactInfo", conexion);
            daCategory.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter pName = new SqlParameter();
            pName.ParameterName = "@Name";
            pName.SqlDbType = SqlDbType.VarChar;
            pName.Value = contactInfo.Name;
            pName.Direction = ParameterDirection.Input;

            SqlParameter pLastName = new SqlParameter();
            pLastName.ParameterName = "@LastName";
            pLastName.SqlDbType = SqlDbType.VarChar;
            pLastName.Value = contactInfo.LastName;
            pLastName.Direction = ParameterDirection.Input;

            SqlParameter pEmail = new SqlParameter();
            pEmail.ParameterName = "@Email";
            pEmail.SqlDbType = SqlDbType.VarChar;
            pEmail.Value = contactInfo.Email ;
            pEmail.Direction = ParameterDirection.Input;

            SqlParameter pMessage = new SqlParameter();
            pMessage.ParameterName = "@Message";
            pMessage.SqlDbType = SqlDbType.VarChar;
            pMessage.Value = contactInfo.Message;
            pMessage.Direction = ParameterDirection.Input;

            daCategory.SelectCommand.Parameters.Add(pName);
            daCategory.SelectCommand.Parameters.Add(pLastName);
            daCategory.SelectCommand.Parameters.Add(pEmail);
            daCategory.SelectCommand.Parameters.Add(pMessage);

            daCategory.Fill(new DataSet(), "a");
            daCategory.Dispose();
        }
    }
}
