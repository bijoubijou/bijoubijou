﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace BijouBijou.DataAccess
{
    public class CategoryDA
    {
        Conexion con = new Conexion();

        public DataSet GetCategoryById(int CategoryId)
        {
            SqlConnection conexion = new SqlConnection();
            conexion = con.conectar();

            SqlDataAdapter daCategory = new SqlDataAdapter("uspGetCategoryById", conexion);
            daCategory.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryId = new SqlParameter();
            pCategoryId.ParameterName = "@CategoryId";
            pCategoryId.SqlDbType = SqlDbType.Int;
            pCategoryId.Value = CategoryId;
            pCategoryId.Direction = ParameterDirection.Input;

            daCategory.SelectCommand.Parameters.Add(pCategoryId);

            DataSet dsCategory = new DataSet();
            daCategory.Fill(dsCategory, "Category");
            return dsCategory;
        }
    }
}
