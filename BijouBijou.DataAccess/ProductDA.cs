﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using BijouBijou.BusinessEntities ;
namespace BijouBijou.DataAccess
{
    public class ProductDA
    {
        Conexion con = new Conexion();

        public List<Product> GetProductsByCategory(int CategoryId)
        {

            SqlConnection conexion = new SqlConnection();
            conexion = con.conectar();

            SqlDataAdapter daProduct = new SqlDataAdapter("uspGetProductsByCategory", conexion);
            daProduct.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryId = new SqlParameter();
            pCategoryId.ParameterName = "@CategoryId";
            pCategoryId.SqlDbType = SqlDbType.Int;
            pCategoryId.Value = CategoryId;
            pCategoryId.Direction = ParameterDirection.Input;

            daProduct.SelectCommand.Parameters.Add(pCategoryId);

            DataSet dsProductList = new DataSet();
            daProduct.Fill(dsProductList, "LoginUsuario");

            var productData = dsProductList.Tables[0].AsEnumerable().Select(p => new Product { 
                ProductId = p.Field<int>("ProductId"),
                ProductCode =p.Field<string>("ProductCode"),
                ProductDesc = p.Field<string>("ProductDesc"),
                ProductDetail = p.Field<string>("ProductDetail"),
                ColorId = p.Field<int>("ColorId"),
                ColorDesc = p.Field<string>("ColorDesc"),
                Price = p.Field<decimal>("Price"),
                Stock = p.Field<int>("Stock"),
                ImageCount = p.Field<int>("ImageCount")
            }); 

            return productData.ToList();
        }


        public DataSet GetProductById(int ProductId) {

            SqlConnection conexion = new SqlConnection();
            conexion = con.conectar();

            SqlDataAdapter daProduct = new SqlDataAdapter("uspGetProductById", conexion);
            daProduct.SelectCommand.CommandType = CommandType.StoredProcedure;

            SqlParameter pCategoryId = new SqlParameter();
            pCategoryId.ParameterName = "@ProductId";
            pCategoryId.SqlDbType = SqlDbType.Int;
            pCategoryId.Value = ProductId;
            pCategoryId.Direction = ParameterDirection.Input;

            daProduct.SelectCommand.Parameters.Add(pCategoryId);

            DataSet dsProduct = new DataSet();
            daProduct.Fill(dsProduct, "Product");
            return dsProduct;
        }
        
    }
}
