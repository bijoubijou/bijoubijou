﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BijouBijou.RetailStore.Models
{
    public class ProductViewModel
    {
        
        public string ProductDesc { get; set;}
        public string ProductDetail { get; set;}
        public decimal Price { get; set; }
        public string Color { get; set; }

    }
}