﻿using System.Web.Mvc;

namespace BijouBijou.RetailStore.Areas.TIENDA
{
    public class TIENDAAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TIENDA";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "TIENDA_default",
                "TIENDA/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
