﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BijouBijou.RetailStore.Areas.TIENDA.Controllers
{
    public class CategoriasController : Controller
    {
        //
        // GET: /TIENDA/Categorias/
        public ActionResult Index(string id)
        {
            ViewBag.Categoria = "";
            
            if (id=="1") {
                ViewBag.Categoria = "Collares";
            }
            else if (id == "2") {
                ViewBag.Categoria = "Brazaletes";
            }
            else if (id == "3")
            {
                ViewBag.Categoria = "Aretes";
            }
            else if (id == "4")
            {
                ViewBag.Categoria = "Tobilleras";
            }

            return View();
        }

    }
}
