(function($) {
    "use strict";

	window.onload = function(){
	    $(".bt-loading").fadeOut("1500", function(){
	        $('#bt_loading').css("display", "none");
	    });
	};

    jQuery(document).ready(function($){
        /* search-form */
        $("#boss_filter_search").selectbox();

        /* header_category */
        loadtopmenu();

        /* header_category menucategory-fixed */
        loadtopmenufixed();
    });

    /* header_category */
    $("#boss-menu-category .boss_heading").click(function() {
        $('#boss-menu-category').toggleClass('opencate');
        loadtopmenu();
    });
    function loadtopmenu() {
        var menuheight = $('#boss-menu-category .box-content').outerHeight();
        var topcate = $('#boss-menu-category').offset().top;
        $('#boss-menu-category .boss-menu-cate .nav_title').each(function(index, element) {
            var liheight = $(this).outerHeight();
            var subheight = $(this).next('.nav_submenu').outerHeight();
            var topheight = $(this).offset().top - topcate - 55;
        });
    }
    $('#boss-menu-category .b_menucategory_hidde,#boss-menu-category  .menu_loadmore_hidden').hide();
    $('#boss-menu-category .menu_loadmore').click(function() {
        $('#boss-menu-category .b_menucategory_hidde').slideToggle("normal", function() {
            $('#boss-menu-category .menu_loadmore').hide();
            $('#boss-menu-category .menu_loadmore_hidden').show();
        });
    });
    $('#boss-menu-category .menu_loadmore_hidden').click(function() {
        $('#boss-menu-category .b_menucategory_hidde').slideToggle("normal", function() {
            $('#boss-menu-category .menu_loadmore').show();
            $('#boss-menu-category .menu_loadmore_hidden').hide();
        });
    });


    /* header_category menucategory-fixed */
    $("#boss-menu-category-fixed .boss_heading").click(function() {
        $('#boss-menu-category-fixed').toggleClass('opencate');
        loadtopmenufixed();
    });
    function loadtopmenufixed() {
        var menuheight = $('#boss-menu-category-fixed .box-content').outerHeight();
        var topcate = $('#boss-menu-category-fixed').offset().top;
        $('#boss-menu-category-fixed .boss-menu-cate .nav_title').each(function(index, element) {
            var liheight = $(this).outerHeight();
            var subheight = $(this).next('#boss-menu-category-fixed .nav_submenu').outerHeight();
            var topheight = $(this).offset().top - topcate - 55;
        });
    }
    $('#boss-menu-category-fixed .b_menucategory_hidde,#boss-menu-category-fixed .menu_loadmore_hidden').hide();
    $('#boss-menu-category-fixed .menu_loadmore').click(function() {
        $('#boss-menu-category-fixed .b_menucategory_hidde').slideToggle("normal", function() {
            $('#boss-menu-category-fixed .menu_loadmore').hide();
            $('#boss-menu-category-fixed .menu_loadmore_hidden').show();
        });
    });
    $('#boss-menu-category-fixed .menu_loadmore_hidden').click(function() {
        $('#boss-menu-category-fixed .b_menucategory_hidde').slideToggle("normal", function() {
            $('#boss-menu-category-fixed .menu_loadmore').show();
            $('#boss-menu-category-fixed .menu_loadmore_hidden').hide();
        });
    });


    /* Scroll Top */
    jQuery(window).scroll(function() {

        /* Header menu */
        $(window).scroll(function(){
            var height_header = $('#top').height() + $('header').height() + $('.boss_header').height() + 10;
            if ($(window).scrollTop() > height_header){
                $('.menu').addClass('boss_scroll');
                $('.boss_header').addClass('boss_scroll');
                $('.header_category').addClass('boss_scroll');
            }else{
                $('.boss_header').removeClass('boss_scroll');
                $('.menu').removeClass('boss_scroll');
                $('.header_category').removeClass('boss_scroll');
            }
        });

        /* header_category menucategory-fixed */
        var height_content = $('#top').height() + $('header').height() + $('.boss_header').height() + $('#boss-menu-category-fixed .box-content').height();
        if ($(window).scrollTop() > height_content) {
            $('#boss-menu-category-fixed .box-content').addClass('show_content');
        } else {
            $('#boss-menu-category-fixed .box-content').removeClass('show_content');
        }

        /* Back Top Setting */
	    if ($(this).scrollTop() > 600) {
	        $('#back_top').fadeIn();
	    } else {
	        $('#back_top').fadeOut();
	    }

	});

	/* Back Top */
	$("#back_top").on('click',function(e) {
	    e.preventDefault();
	    $('body,html').animate({
	        scrollTop: 0
	    }, 800, 'swing');
	});

})(jQuery);

function getMaxHeight($elms){
    var maxHeight = 0;
    $($elms).each(function()
    {
        var height = $(this).outerHeight();
        if (height > maxHeight)
        {
            maxHeight = height;
        }
    });
    return maxHeight;
};