﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BijouBijou.BusinessEntities
{
    public class Color
    {
        public int ColorId { get; set; }
        public string ColorDesc { get; set; }
        public string ColorDescEn { get; set; }
    }
}
