﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BijouBijou.BusinessEntities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductDesc{ get; set; }
        public string ProductDetail{ get; set; }
        public decimal Price{ get; set; }
        public int Stock{ get; set; }
        public int ColorId{ get; set; }
        public string ColorDesc { get; set; }
        public int CategoryId{ get; set; }
        public int ImageCount { get; set; }

    }
}
