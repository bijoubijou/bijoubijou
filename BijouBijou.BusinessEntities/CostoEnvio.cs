﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BijouBijou.BusinessEntities
{
    public class CostoEnvio
    {
        public string ZonaReparto { get; set; }
        public string Costo { get; set; }
    }
}
