﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BijouBijou.BusinessEntities
{
    public class Category
    {
        public int CategoryId { get; set; }
        public string CategoryDesc { get; set; }
        public string CategoryDescEn { get; set; }
        public List<Product> ProductList { get; set; } 
    }
}
